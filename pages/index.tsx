import { useState } from "react";
import { Dialog } from "@headlessui/react";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import Image from "next/image";
import Footer from "@/components/Footer";
import Header from "@/components/Header";
import Link from "next/link";

const people = [
  {
    name: "Өдрийн амралт",
    imageUrl: "/busad_hutulbur_1.jpeg",
  },
  {
    name: "Гэр бүлийн амралт",
    imageUrl: "/busad-hutulbur-2.png",
  },
  {
    name: "Театр, танхимын түрээсийн үйлчилгээ",
    imageUrl: "/busad_hutulbur_3.png",
  },
  {
    name: "Оюутан залууст",
    imageUrl: "/busad_hutulbur_4.png",
  },
  // More people...
];

const posts = [
  {
    id: 1,
    title: "“Хамтдаа инээмсэглэе” тусгай аян",
    href: "#",
    description:
      "Хөдөлмөр, нийгмийн хамгааллын яам, Гэр бүл хүүхэд, залуучуудын хөгжлийн газар, Хөдөлмөр, халамжийн үйлчилгээний ерөнхий газрын дэмжлэгтэйгээр “Ээлтэй Ертөнц” ТББ-ын санаачлагаар хүнд хэлбэрийн хөгжлийн бэрхшээлтэй, байнгын асаргаа...",
    imageUrl:
      "https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3603&q=80",
  },
  {
    id: 2,
    title: "“Хил хязгааргүй номын сан” төсөл",
    href: "#",
    description:
      "Австралийн Элчин сайдын яам, Монгол Улсын Үндэсний номын сан, Монголын номын сангийн холбоотой хамтарсан “Хил хязгааргүй номын сан” төслийг Найрамдал цогцолборт хэрэгжүүлэхээр Австрали улсын Элчин сайдын хэргийг түр хамаарагч Дэйвид Прэстон, Австралийн...",
    imageUrl:
      "https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3603&q=80",
  },
  {
    id: 3,
    title: "2023 оны үйл ажиллагаа “МАЙХАНТ” зуслангаар эхэллээ",
    href: "#",
    description:
      "Хайрт хүүхдүүд, хүндэт эцэг эхчүүд ээ! Та бүхэндээ шинэ оны мэнд хүргэж байна. 2023 оны үйл ажиллагаа Баянгол дүүргийн скаут хүүхдүүдийн 'Индоор камп' арга хэмжээгээр эхэлж байна...",
    imageUrl:
      "https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3603&q=80",
  },
];

const stats = [
  { id: 1, name: "Үндсэн ээлж", value: "10 900 +" },
  { id: 2, name: "Өдрийн амрагч", value: "24 200 +" },
  { id: 3, name: "Нийт амрагч", value: "40 000 +" },
];

export default function Example() {
  return (
    <>
      <div className="bg-white">
        <div className="relative isolate pt-14">
          <div
            className="absolute inset-x-0 -top-40 -z-10 transform-gpu overflow-hidden blur-3xl sm:-top-80"
            aria-hidden="true"
          >
            <div
              className="relative left-[calc(50%-11rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-30rem)] sm:w-[72.1875rem]"
              style={{
                clipPath:
                  "polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)",
              }}
            />
          </div>
          <div className="py-24 sm:py-32 lg:pb-40">
            <div className="mx-auto max-w-7xl px-6 lg:px-8">
              <div className="mx-auto max-w-6xl text-center">
                <h1 className="text-4xl font-bold tracking-tight text-gray-900 sm:text-6xl">
                  Найрамдал олон улсын хүүхдийн зусланд тавтай морил!
                </h1>
                <p className="mt-6 text-lg leading-8 text-gray-600 text-center sm:px-36">
                  Хүүхдийн үерхэл нөхөрлөлийн хотхон “Найрамдал” төв нь та
                  бүхнийг ая тухтай амрах орчныг бүрдүүлж хөгжих, оролцох эрхийг
                  тань хангахад чиглэгдсэн хөгжлийн хөтөлбөрүүдээр үйлчилж
                  байна.
                </p>
                <div className="mt-10 flex items-center justify-center gap-x-6">
                  <Link
                    href="/eelj"
                    className="rounded-full bg-indigo-600 px-3.5 py-2.5 text-sm font-semibold text-[white] shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                  >
                    Ээлжийн мэдээлэл
                  </Link>
                  <a
                    href="#"
                    className="text-sm font-semibold leading-6 text-gray-900 border-indigo-600 border-2 py-1.5 px-8 rounded-full"
                  >
                    Бүртгүүлэх
                  </a>
                </div>
              </div>
              <div className="mt-16 sm:mt-24">
                <div className="-m-2 lg:-m-4 lg:rounded-2xl lg:p-4">
                  <Image
                    src="/home.png"
                    alt="App screenshot"
                    width={2432}
                    height={1442}
                    className="rounded-md  ring-gray-900/10"
                  />
                </div>
              </div>
            </div>
          </div>
          <div
            className="absolute inset-x-0 top-[calc(100%-13rem)] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[calc(100%-30rem)]"
            aria-hidden="true"
          >
            <div
              className="relative left-[calc(50%+3rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%+36rem)] sm:w-[72.1875rem]"
              style={{
                clipPath:
                  "polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)",
              }}
            />
          </div>
        </div>
      </div>
      {/* Stats */}
      <div className="bg-white py-24 sm:py-32">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          <div className="mx-auto max-w-2xl lg:max-w-none">
            <div className="text-center">
              <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
                Манайд амрагчдын тоо
              </h2>
              <p className="mt-4 text-lg leading-8 text-gray-600">
                Үндсэн ээлж болон өдрийн амрагч гэсэн 2 хөтөлбөрөөр амрагчдыг
                хүлээн авч, чанартай хүртээмжтэй үйлчилгээг үзүүлж байна.
              </p>
            </div>
            <dl className="mt-16 grid grid-cols-1 gap-0.5 overflow-hidden rounded-2xl text-center sm:grid-cols-2 lg:grid-cols-3">
              {stats.map((stat) => (
                <div key={stat.id} className="flex flex-col bg-gray-400/5 p-8">
                  <dt className="text-sm font-semibold leading-6 text-gray-600">
                    {stat.name}
                  </dt>
                  <dd className="order-first text-3xl font-semibold tracking-tight text-gray-900">
                    {stat.value}
                  </dd>
                </div>
              ))}
            </dl>
          </div>
        </div>
      </div>
      {/* undsen eelj */}

      <div className="bg-white">
        <div className="relative isolate overflow-hidden bg-gradient-to-b from-indigo-100/20">
          <div className="mx-auto max-w-7xl pb-24 pt-10 sm:pb-32 lg:grid lg:grid-cols-2 lg:gap-x-8 lg:px-8 lg:py-40">
            <div className="px-6 lg:px-0 lg:pt-4">
              <div className="mx-auto max-w-2xl">
                <div className="max-w-lg">
                  <h1 className="mt-10 text-4xl font-bold tracking-tight text-gray-900 sm:text-6xl">
                    Үндсэн ээлжүүд
                  </h1>
                  <p className="mt-6 text-lg leading-8 text-gray-600">
                    Тус цогцолбор нь жилийн туршид 3, 5, 7, 10 хоногоор 40-45
                    үндсэн ээлж зохион байгуулдаг ба үндсэн ээлжээ хавар, намар,
                    зун, өвөл гэсэн хэлбэрээр зохион байгуулдаг.
                  </p>
                  <div className="mt-10 flex items-center gap-x-6">
                    <a
                      href="#"
                      className="rounded-full bg-indigo-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                    >
                      Ээлжийн мэдээлэл
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="mt-20 sm:mt-24 md:mx-auto md:max-w-2xl lg:mx-0 lg:mt-0 lg:w-screen">
              <div
                className="absolute inset-y-0 right-1/2 -z-10 -mr-10 w-[200%] skew-x-[-30deg] bg-white shadow-xl shadow-indigo-600/10 ring-1 ring-indigo-50 md:-mr-20 lg:-mr-36"
                aria-hidden="true"
              />
              <Image
                src="/undsen_eelj.png"
                width={1200}
                height={600}
                alt=""
                className="p-4 rounded-md"
              />
            </div>
          </div>
          <div className="absolute inset-x-0 bottom-0 -z-10 h-24 bg-gradient-to-t from-white sm:h-32" />
        </div>
      </div>

      {/* Teams */}

      <div className="bg-gray-200 py-24 sm:py-32">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          <div className="mx-auto max-w-2xl lg:mx-0">
            <h2 className="text-3xl font-bold tracking-tight text-black sm:text-4xl">
              Бусад хөтөлбөр, үйлчилгээ
            </h2>
            <p className="mt-6 text-lg leading-8 text-gray-600">
              Бид үндсэн ээлжээс гадна дараах үйлчилгээг анги хамт олон, оюутан
              залуус, гэр бүл болон албан байгууллагад үзүүлж байна.
            </p>
          </div>
          <ul
            role="list"
            className="mx-auto mt-20 grid max-w-2xl grid-cols-1 gap-x-8 gap-y-14 sm:grid-cols-2 lg:mx-0 lg:max-w-none lg:grid-cols-3 xl:grid-cols-4"
          >
            {people.map((person) => (
              <li key={person.name}>
                <img
                  className="aspect-[14/13] w-full rounded-2xl object-cover"
                  src={person.imageUrl}
                  alt=""
                />
                <h3 className="mt-3 text-lg font-semibold leading-8 tracking-tight text-[#003463]">
                  {person.name}
                </h3>
                <div className="mt-6">
                  <a
                    href="#"
                    className="mt-6 rounded-full border-2 border-[#07B2EC] px-5 py-2 text-sm font-semibold text-[#07B2EC] "
                  >
                    Дэлгэрэнгүй
                  </a>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
      {/* Blogs */}

      <div className="bg-white py-24 sm:py-32">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          <div className="mx-auto max-w-2xl text-center">
            <h2 className="text-3xl font-bold tracking-tight text-[#003463] sm:text-4xl">
              Мэдээ, мэдээлэл
            </h2>
          </div>
          <div className="mx-auto mt-16 grid max-w-2xl grid-cols-1 gap-x-8 gap-y-20 lg:mx-0 lg:max-w-none lg:grid-cols-3">
            {posts.map((post) => (
              <article
                key={post.id}
                className="flex flex-col items-start justify-between"
              >
                <div className="relative w-full">
                  <img
                    src={post.imageUrl}
                    alt=""
                    className="aspect-[16/9] w-full rounded-2xl bg-gray-100 object-cover sm:aspect-[2/1] lg:aspect-[3/2]"
                  />
                  <div className="absolute inset-0 rounded-2xl ring-1 ring-inset ring-gray-900/10" />
                </div>
                <div className="max-w-xl">
                  <div className="group relative">
                    <h3 className="mt-3 text-lg font-semibold leading-6 text-[#003463] ">
                      <a href={post.href}>
                        <span className="absolute inset-0" />
                        {post.title}
                      </a>
                    </h3>
                    <p className="mt-5 line-clamp-4 text-sm leading-6 text-[#001447]">
                      {post.description}
                    </p>
                  </div>
                </div>
              </article>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
