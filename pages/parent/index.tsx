import { Disclosure } from "@headlessui/react";
import { MinusSmallIcon, PlusSmallIcon } from "@heroicons/react/24/outline";

const faqs = [
  {
    title: "Манай цогцолборт амрах хугацаандаа:",
    question: "Амрагч хүүхдийн мөрдөх журам",
    answer:
      " QR код бүхий эрхийн бичгийг хаяж, гээгдүүлэхгүй байх / Бусдыг хүндэтгэн харилцааны эерэг уур амьсгалыг эрхэмлэх / Зуслангийн хөтөлбөр, үйл ажиллагаанд цагаа баримтлан идэвхи санаачлагатай оролцох / Бүлгийн удирдагчид болон зуслангийн ажилчдыг хүндлэх / Бусад хүүхдүүдтэй хамтдаа найзалж нөхөрлөн , бусдад туслах / Цогцолборын эд зүйлс болон байгальд хайр гамтай хандах / Аливаа осол гэмтлээс өөрийгөө урьдчилан сэргийлэх",
  },
  {
    title: "",
    question: "Зусланд юу авч явах вэ?",
    answer:
      "Хүүхэддээ үнэтэй , чамин хувцас, гар утас, видео камер, аппарат, алт үнэт эдлэл авч явахгүй байхыг анхааруулаарай. Зайлшгүй авч ирэх тохиолдолд эд зүйлсээ гээсэн, алдсан тохиолдолд зуслангаас хариуцлага хүлээхгүй болно / Манай цогцолборт тусгай дүрэмт хувцас өмсөх шаардлагагүй хэдий ч амрагчид бүхэл өдрийн турш идэвхитэй байдаг тул дотор болон гадаа өмсөх биед эвтэйхэн хувцас, дулаан хувцас (Хүрэм, ноосон цамц, ємд, борооны цув, бусад) өгч явуулаарай. Мөн өөрийн хувцсандаа хайр гамтай байж, гээхгүй, орхихгүй байхыг сануулаарай. / Ариун цэврийн хэрэглэл (шүдний оо, сойз, гар нүүрийн алчуур шампунь сам , бусад), өрөөний болон усны шаахай, зэргийг бэлтгэсэн байх шаардлагатайг анхаараарай. / Амны хаалт, гар ариутгагч заавал өгч явуулна. / Хүүхдийнхээ суурь євчний талаарх мэдээллийг єгч, нэн шаардлагатай эмийг өгч явуулаарай.",
  },
  {
    title: "",
    question: "Хориглох зүйлс",
    answer:
      "Тэжээвэр амьтан / Гал асаагуур, ил гал асаах, лаа, үсний индүү / Ямар ч төрлийн буу (усан буу, үрлэн сумтай буу) эсвэл зэвсэг (хурц иртэй, хайч, хутга, үзүүртэй зүйлс, цаасны хутга) / Хууль бус бодисууд / Архи, тамхи, цахилгаан тамхи / Үнэтэй эд эдлэл (дрон, зургийн аппарат, их хэмжээний бэлэн мөнгө, зөөврийн таблет эсвэл компьютер) / Лазераар заагч гэрэл / Муу хандлага / Хог хаягдал ил хаях / Ой мод болон бусад хэсэг рүү багшийн зөвшөөрөлгүй явах / Хурдан муудах хүнсний бүтээгдэхүүн (мах махан бүтээгдэхүүн, лаазалсан бүтээгдэхүүн, өндөгний орцтой бүтээгдэхүүн)",
  },
  // More questions...
];

const faqsEnd = [
  {
    question: "Автобусны аюулгүй байдлын дүрэм юу вэ?",
    answer: `Суудлын бүсийг заавал зүүх Автобус хөдлөх үед босохгүй байх Автобусанд юм идэх хориотой Суудал дээрээс эргэж харах, босож зогсох, үсрэхгүй байх  Бүлгийн удирдагчийн заавар зөвлөгөөг биелүүлэх`,
  },
  {
    question: "Хоцорсон үед яах вэ?",
    answer:
      "Ээлжийн цуваанаас хоцорсон тохиолдолд эцэг эхчүүд болон асран хамгаалагч нь цогцолборын байранд хүргэж өгөх асуудлыг бүрэн хариуцна. Амрагч хүүхдийн овог нэр, нас, хүйс, холбоо барих дугаар, цогцолборт ирэх цаг зэргийг тухайн өдрийн 12.00 цагаас өмнө 75056010 дугаарт залган тэмдэглүүлнэ. Заасан хугацаанд дээрх мэдээллийг ирүүлээгүй тохиолдолд бүлэг хувиарлалтаас хоцрох тул хариуцлагатай хандахыг хүсч байна.",
  },
  {
    question: "Эргэлт хийх бол яах вэ?",
    answer:
      "Зуслан нь хүүхдийн бие даах чадвар нэмэгдүүлэх зорилготой орчин учраас “ЭРГЭЛТ” үйл ажиллагааг зохион байгуулахгүйг анхаарна уу.",
  },
  {
    question: "Бүлгийн мэдээлэл хэрхэн авах вэ?",
    answer:
      "Та хүүхдээ аль бүлэгт орсон, бүлгийн удирдагчтай холбогдох тохиолдолд тухайн ээлж эхэлсэн өдрийн 14,00 цагаас хойш 70496166 тоот утсаар хүүхдийнхээ овог, нэр, сургуулийг хэлж ямар бүлэгт хувиарлагдсаныг мэдэж болно.",
  },
  // More questions...
];

export default function Parent() {
  return (
    <div className="bg-white">
      <div className="mx-auto max-w-7xl px-6 py-24 sm:py-32 lg:px-8 lg:py-40">
        <div className="mx-auto max-w-4xl divide-y divide-gray-900/10">
          <h2 className="text-2xl font-bold leading-10 tracking-tight text-gray-900">
            Эцэг эхийн булан
          </h2>
          <dl className="mt-10 space-y-6 divide-y divide-gray-900/10">
            {faqs.map((faq) => (
              <Disclosure as="div" key={faq.question} className="pt-6">
                {({ open }) => (
                  <>
                    <dt>
                      <Disclosure.Button className="flex w-full items-start justify-between text-left text-gray-900">
                        <span className="font-semibold leading-7 text-lg">
                          {faq.question}
                        </span>
                        <span className="ml-6 flex h-7 items-center">
                          {open ? (
                            <MinusSmallIcon
                              className="h-6 w-6"
                              aria-hidden="true"
                            />
                          ) : (
                            <PlusSmallIcon
                              className="h-6 w-6"
                              aria-hidden="true"
                            />
                          )}
                        </span>
                      </Disclosure.Button>
                    </dt>
                    <Disclosure.Panel as="dd" className="mt-2 pr-12 ml-10">
                      <p className="text-base text-gray-600">{faq.title}</p>
                      <ul className="text-base leading-7 text-gray-600">
                        {faq.answer.split("/").map((item) => (
                          <li key={item} className="list-disc">
                            {item}
                          </li>
                        ))}
                      </ul>
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
            ))}
          </dl>
        </div>
      </div>
      <div className="mx-auto max-w-7xl px-6 py-24 sm:pt-32 lg:px-8 lg:py-40">
        <div className="lg:grid lg:grid-cols-12 lg:gap-8">
          <div className="lg:col-span-5">
            <h2 className="text-2xl font-bold leading-10 tracking-tight text-gray-900">
              Түгээмэл асуулт хариултууд
            </h2>
          </div>
          <div className="mt-10 lg:col-span-7 lg:mt-0">
            <dl className="space-y-10">
              {faqsEnd.map((faq) => (
                <div key={faq.question}>
                  <dt className="text-base font-semibold leading-7 text-gray-900">
                    {faq.question}
                  </dt>
                  <dd className="mt-2 text-base leading-7 text-gray-600">
                    {faq.answer}
                  </dd>
                </div>
              ))}
            </dl>
          </div>
        </div>
      </div>
    </div>
  );
}
