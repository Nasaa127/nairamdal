import { Fragment, useState } from "react";
import { Dialog, Popover, Tab, Transition } from "@headlessui/react";
import {
  Bars3Icon,
  MagnifyingGlassIcon,
  ShoppingCartIcon,
  UserIcon,
  XMarkIcon,
} from "@heroicons/react/24/outline";
import { ChevronDownIcon } from "@heroicons/react/20/solid";
import Link from "next/link";

const products = [
  {
    id: 1,
    name: "Томруулдаг шил /Magnifying  Glass/",
    title: "1-р Ээлж",
    description:
      "Мөрдөгчийн дүрд хувирсан  хүүхдүүд зуслан дээр нууц  кодуудыг тайлах танин мэдэхүй,  шинжлэх ухааны хөтөлбөрт ээлж.",
    href: "#",
    price: "231,000",
    step: 4,
    date: "06.03 - 06.08  /5 хоног/",
    age: "6-10 нас",
    imageSrc: "/1r-eelj.png",
    imageAlt: "Insulated bottle with white base and black snap lid.",
  },
  {
    id: 2,
    name: "Би Монгол хүн /I am Mongolian/",
    title: "2-р Ээлж",
    description:
      "Эх орноо  хайрлах, Монгол өв  уламжлалаа дээдлэх үзлийг төлөвшүүлж эрүүл чийрэг, эх оронч сэтгэлгээтэй ирээдүйн эздийг бэлдэхэд чиглэсэн хөтөлбөрт ээлж.",
    href: "#",
    price: "318,000",
    step: 0,
    date: "06.03 - 06.08  /5 хоног/",
    age: "6-12 нас",
    imageSrc: "/2r-eelj.png",
    imageAlt:
      "Arm modeling wristwatch with black leather band, white watch face, thin watch hands, and fine time markings.",
  },
  {
    id: 3,
    name: "Хүмүүнлэг иргэн -  Хөгжлийн ирээдүй /Humanitarian - Future development/",
    title: "3-р Ээлж",
    description:
      "Хүүхэд, өсвөр үеийнхнийг хүмүүнлэгийн үйлсэд уриалан оролцуулж, хүнлэг энэрэнгүй үзэл санааг төлөвшүүлж, тэдний оролцоонд тулгуурласан хөтөлбөрт ээлж.",
    href: "#",
    price: "318,000",
    step: 2,
    date: "06.03 - 06.08  /5 хоног/",
    age: "10 - 17 нас",
    imageSrc: "/3r-eelj.png",
    imageAlt:
      "Arm modeling wristwatch with black leather band, white watch face, thin watch hands, and fine time markings.",
  },
];

function classNames(...classes: any[]) {
  return classes.filter(Boolean).join(" ");
}

export default function Example() {
  const [open, setOpen] = useState(false);

  return (
    <div className="bg-gray-50 pt-20">
      <main className="mx-auto max-w-2xl pb-24 pt-8 sm:px-6 sm:pt-16 lg:max-w-7xl lg:px-8">
        <div className="space-y-2 px-4 sm:flex sm:items-baseline sm:justify-between sm:space-y-0 sm:px-0">
          <div className="flex sm:items-baseline sm:space-x-4">
            <h1 className="text-2xl font-bold tracking-tight text-gray-900 sm:text-3xl">
              Үндсэн ээлжүүд
            </h1>
          </div>
          <p>Нэвтэрсний дараагаар ээлжийн тасалбар худалдан авах боломжтой.</p>
        </div>

        {/* Products */}
        <section aria-labelledby="products-heading" className="mt-6">
          <h2 id="products-heading" className="sr-only">
            Products purchased
          </h2>

          <div className="space-y-8">
            {products.map((product) => (
              <>
                <div className="flex gap-6 items-center">
                  <p className="font-bold text-lg mx-4">{product.title}</p>
                  <Link
                    href={`/eelj/${product.id}`}
                    className="underline text-cyan-300 text-sm"
                  >
                    Дэлгэрэнгүй
                  </Link>
                </div>
                <a
                  href={`/eelj/${product.id}`}
                  key={product.id}
                  className="border-b border-t border-gray-200 bg-white shadow-sm sm:rounded-lg sm:border"
                >
                  <div className="px-4 py-6 sm:px-6 lg:grid lg:grid-cols-12 lg:gap-x-8 lg:p-8">
                    <div className="sm:flex lg:col-span-7">
                      <div className="aspect-h-1 aspect-w-1 w-full flex-shrink-0 overflow-hidden rounded-lg sm:aspect-none sm:h-40 sm:w-40">
                        <img
                          src={product.imageSrc}
                          alt={product.imageAlt}
                          className="h-full w-full object-cover object-center sm:h-full sm:w-full"
                        />
                      </div>

                      <div className="mt-6 sm:ml-6 sm:mt-0">
                        <h3 className="text-base font-medium text-gray-900">
                          <a href={product.href}>{product.name}</a>
                        </h3>
                        <p className="mt-2 text-sm font-medium text-gray-900">
                          {product.price}₮
                        </p>
                        <p className="mt-3 text-sm text-gray-500">
                          {product.description}
                        </p>
                      </div>
                    </div>

                    <div className="mt-6 lg:col-span-5 lg:mt-0">
                      <dl className="grid grid-cols-2 gap-x-6 text-sm">
                        <div>
                          <dt className="font-medium text-gray-900">
                            Насны ангилал
                          </dt>
                          <dd className="mt-3 text-gray-500">
                            <span className="block">{product.age}</span>
                          </dd>
                        </div>
                        <div>
                          <dt className="font-medium text-gray-900">Хугацаа</dt>
                          <dd className="mt-3 space-y-3 text-gray-500">
                            <p>{product.date}</p>
                          </dd>
                          <button className="px-4 bg-gray-400 rounded-full text-white py-2 mt-8">
                            Худалдаж авах
                          </button>
                        </div>
                      </dl>
                    </div>
                  </div>

                  <div className="border-t border-gray-200 px-4 py-6 sm:px-6 lg:p-8">
                    <div className="mt-6" aria-hidden="true">
                      <div className="mb-6 hidden grid-cols-4 text-sm font-medium text-gray-600 sm:grid">
                        <div
                          className={classNames(
                            product.step >= 0 ? "text-indigo-600" : "",
                            "text-left"
                          )}
                        >
                          {product.step === 2
                            ? "Бүртгэл явагдаж байна"
                            : product.step > 2
                            ? "Бүртгэл дууссан"
                            : "Бүртгэл 7 хоногийн дараа эхэлнэ"}
                        </div>
                      </div>
                      <div className="overflow-hidden rounded-full bg-gray-200">
                        <div
                          className="h-2 rounded-full bg-indigo-600"
                          style={{
                            width: `calc((${product.step} * 2) / 8 * 100%)`,
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </a>
              </>
            ))}
          </div>
        </section>
      </main>
    </div>
  );
}
