/*
  This example requires some changes to your config:
  
  ```
  // tailwind.config.js
  module.exports = {
    // ...
    plugins: [
      // ...
      require('@tailwindcss/typography'),
      require('@tailwindcss/aspect-ratio'),
    ],
  }
  ```
*/
import { useState } from "react";
import { Disclosure, RadioGroup, Tab } from "@headlessui/react";
import { StarIcon } from "@heroicons/react/20/solid";
import { HeartIcon, MinusIcon, PlusIcon } from "@heroicons/react/24/outline";

const product = {
  name: "Сорилтонд бэлэн /Challange  Accepted/",
  price: "316,500",
  date: "06.18 - 06.24  /6 хоног/",
  age: "14-15 нас",
  images: [
    {
      id: 1,
      name: "Angled view",
      src: "/Rectangle_63.png",
      alt: "Angled front view with bag zipped and handles upright.",
    },
    // More images...
  ],
  description: `
    <p>Мөрдөгчийн дүрд хувирсан  хүүхдүүд зуслан дээр нууц  кодуудыг тайлах танин мэдэхүй, шинжлэх ухааны хөтөлбөрт ээлж.</p>
  `,
  details: [
    {
      name: "Төвлөрсөн арга хэмжээ",
      description:
        "Зусланд амарч буй нийт бүлгийн хүүхдүүд хамрагдах тэмцээн уралдаан, зохион байгуулалтай төвлөрсөн үйл ажиллагаа юм.",
      items: [
        "Мөнгөн хараацай: Урлагийн хөтөлбөр. Урлагийн төрөлжсөн тэмцээн уралдаан зохион байгуулдаг хөтөлбөр",
        "Спорт өдөрлөг: спортын олон төрөлт, төрөлжисөн тэмцээн уралдаан зохион байгуулах хөтөлбөр",
        " Танин мэдэхүйн АСК тэмцээн: Танин мэдэхүй чиглэлийн уралдаан тэмцээнүүд /IQ, танин мэдэхүйн асуулт хариулт, хөгжилтэй тэмцээн, 60 секунд, аяыг та, үг бүтээх зэрэг тэмцээнээс бүрдэнэ.",
        "Байгалийн аялал: Байгаль экологи, ургамал амьтан, шавж хорхойн талаархи мэдээлэлийг өртөөчилсөн хэлбэрээр зохион байгуулах хөтөлбөр",
        "Тоглоомын тойрог: Амрагчдын чөлөөт цагаар хамт олон багийн ажиллагааг төлөвшүүлэх олон төрлийн тоглоом тоглуулах хөтөлбөр.",
        "Нэгдсэн өглөөний дасгал: Ээлжин бүх амрагчдийн өглөө бүр хийдэг бие бялдар, эрүүл мэндийн ач холбогдолтой дасгалууд",
        "Өртөөчилсөн ажилууд: Зан заншил, амдрах ухаан, эрүүл мэнд, харицаа, эрх үүрэг зэрэг сэдэвээр хүүхэд багачуудад мэдээлэл өгөхч өртөөчилсөн үйл ажиллагаа.",
        "Нээлт хаалтын /Туг мандуулах, буулгах/ ажиллагаа: Ээлж бүрийн эхний болон сүүлийн өдөр зоаион байгуулагдах албан ёсны хөтөлбөр зэрэг 10 гаруй хөтөлбөр үйл ажиллагаа тогтмол зохион байгуулагдаж байна.",
      ],
    },
    {
      name: "Features",
      items: [
        "Мөнгөн хараацай: Урлагийн хөтөлбөр. Урлагийн төрөлжсөн тэмцээн уралдаан зохион байгуулдаг хөтөлбөр",
        "Спорт өдөрлөг: спортын олон төрөлт, төрөлжисөн тэмцээн уралдаан зохион байгуулах хөтөлбөр",
        " Танин мэдэхүйн АСК тэмцээн: Танин мэдэхүй чиглэлийн уралдаан тэмцээнүүд /IQ, танин мэдэхүйн асуулт хариулт, хөгжилтэй тэмцээн, 60 секунд, аяыг та, үг бүтээх зэрэг тэмцээнээс бүрдэнэ.",
        "Байгалийн аялал: Байгаль экологи, ургамал амьтан, шавж хорхойн талаархи мэдээлэлийг өртөөчилсөн хэлбэрээр зохион байгуулах хөтөлбөр",
        "Тоглоомын тойрог: Амрагчдын чөлөөт цагаар хамт олон багийн ажиллагааг төлөвшүүлэх олон төрлийн тоглоом тоглуулах хөтөлбөр.",
        "Нэгдсэн өглөөний дасгал: Ээлжин бүх амрагчдийн өглөө бүр хийдэг бие бялдар, эрүүл мэндийн ач холбогдолтой дасгалууд",
        "Өртөөчилсөн ажилууд: Зан заншил, амдрах ухаан, эрүүл мэнд, харицаа, эрх үүрэг зэрэг сэдэвээр хүүхэд багачуудад мэдээлэл өгөхч өртөөчилсөн үйл ажиллагаа.",
        "Нээлт хаалтын /Туг мандуулах, буулгах/ ажиллагаа: Ээлж бүрийн эхний болон сүүлийн өдөр зоаион байгуулагдах албан ёсны хөтөлбөр зэрэг 10 гаруй хөтөлбөр үйл ажиллагаа тогтмол зохион байгуулагдаж байна.",
      ],
    },
  ],
  project: [
    {
      title: "Байгаль танин мэдэхүй /World Challange/",
      description: "",
      items:
        "Би байгалийн найз / Хог хаягдалын менежмент / Цэцэгчний хүслэн / Хиймэл хананы авиралт / Уяа зангилаа / Уулын дугуйтай аялал",
    },
    {
      title: "Зан заншил, Өв соёл, Уламжлал /Go Nomad/",
      description: "",
      items:
        "Миний нутаг / Эх хэлний баялаг / Монгол гэр / Оньсон тоглоом / Төрийн бэлгэдэл / Эрийн гурван наадам / Морин хуур / Бие биелэгээ / Морь унах / Нум сум харвах",
    },
    {
      title: "Амьдрах ухаан, Бүтээлч сэтгэлгээ /My Dream, My Promise/",
      description: "",
      items:
        "Керамик урлал / Шүрэн урлал / Утсан зангилаа / Модон урлал / Арьсан урлал / Санхүүгийн боловсрол",
    },
    {
      title: "Адал явдал гадаад орчин /Are you ready/",
      description: "",
      items:
        "Хэмнэл / Гитар, Үкүлэлэ хөгжим / Бүжиг (Чөлөөт, цэнгээнт) / Найрамдал медиа / Спорт өрөлт / Кендама / Крикет / Райгби / Гар бөмбөг / Сагсан бөмбөг / Хөл бөмбөг",
    },
    {
      title: "Соёл урлаг, спорт /My Hobby/",
      description: "",
      items:
        "Хэмнэл / Гитар, Үкүлэлэ хөгжим / Бүжиг (Чөлөөт, цэнгээнт) / Найрамдал медиа / Спорт өрөлт / Кендама / Крикет / Райгби / Гар бөмбөг / Сагсан бөмбөг / Хөл бөмбөг",
    },
  ],
};

const faqs = [
  {
    question: "Автобусны аюулгүй байдлын дүрэм юу вэ?",
    answer:
      "Суудлын бүсийг заавал зүүх Автобус хөдлөх үед босохгүй байх Автобусанд юм идэх хориотой Суудал дээрээс эргэж харах, босож зогсох, үсрэхгүй байх Бүлгийн удирдагчийн заавар зөвлөгөөг биелүүлэх",
  },
  {
    question: "Хоцорсон үед яах вэ?",
    answer:
      "Ээлжийн цуваанаас хоцорсон тохиолдолд эцэг эхчүүд болон асран хамгаалагч нь цогцолборын байранд хүргэж өгөх асуудлыг бүрэн хариуцна. Амрагч хүүхдийн овог нэр, нас, хүйс, холбоо барих дугаар, цогцолборт ирэх цаг зэргийг тухайн өдрийн 12.00 цагаас өмнө 75056010 дугаарт залган тэмдэглүүлнэ. Заасан хугацаанд дээрх мэдээллийг ирүүлээгүй тохиолдолд бүлэг хувиарлалтаас хоцрох тул хариуцлагатай хандахыг хүсч байна.",
  },
  {
    question: "Эргэлт хийх бол яах вэ?",
    answer:
      "Зуслан нь хүүхдийн бие даах чадвар нэмэгдүүлэх зорилготой орчин учраас “ЭРГЭЛТ” үйл ажиллагааг зохион байгуулахгүйг анхаарна уу.",
  },
  {
    question: "Бүлгийн мэдээлэл хэрхэн авах вэ?",
    answer:
      "Та хүүхдээ аль бүлэгт орсон, бүлгийн удирдагчтай холбогдох тохиолдолд тухайн ээлж эхэлсэн өдрийн 14,00 цагаас хойш 70496166 тоот утсаар хүүхдийнхээ овог, нэр, сургуулийг хэлж ямар бүлэгт хувиарлагдсаныг мэдэж болно..",
  },
];

function classNames(...classes: any[]) {
  return classes.filter(Boolean).join(" ");
}

export default function Example() {
  return (
    <div className="bg-white">
      <div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
        <div className="lg:grid lg:grid-cols-2 lg:items-start lg:gap-x-8">
          <Tab.Group as="div" className="flex flex-col-reverse">
            {/* Image selector */}
            {/* <div className="mx-auto mt-6 hidden w-full max-w-2xl sm:block lg:max-w-none">       <Tab.List className="grid grid-cols-4 gap-6"> {product.images.map((image) => (   <Tab     key={image.id}     className="relative flex h-24 cursor-pointer items-center justify-center rounded-md bg-white text-sm font-medium uppercase text-gray-900 hover:bg-gray-50 focus:outline-none focus:ring focus:ring-opacity-50 focus:ring-offset-4"   >     {({ selected }) => (       <> <span className="sr-only"> {image.name} </span> <span className="absolute inset-0 overflow-hidden rounded-md">   <img     src={image.src}     alt=""     className="h-full w-full object-cover object-center"   /> </span> <span   className={classNames(     selected ? "ring-indigo-500" : "ring-transparent",     "pointer-events-none absolute inset-0 rounded-md ring-2 ring-offset-2"   )}   aria-hidden="true" />       </>     )}   </Tab> ))}       </Tab.List>     </div> */}
            <Tab.Panels className="aspect-h-1 aspect-w-1 w-full">
              {product.images.map((image) => (
                <Tab.Panel key={image.id}>
                  <img
                    src={image.src}
                    alt={image.alt}
                    className="h-full w-full object-cover object-center sm:rounded-lg"
                  />
                </Tab.Panel>
              ))}
            </Tab.Panels>
          </Tab.Group>
          {/* Product info */}
          <div className="mt-10 px-4 sm:mt-16 sm:px-0 lg:mt-0">
            <h1 className="text-3xl font-bold tracking-tight text-gray-900">
              {product.name}
            </h1>
            <div className="mt-3">
              <h2 className="sr-only">Product information</h2>
              <p className="text-3xl tracking-tight text-gray-900">
                {product.price} ₮
              </p>
            </div>
            <div className="mt-6">
              <h3 className="sr-only">Description</h3>
              <div
                className="space-y-6 text-base text-gray-700"
                dangerouslySetInnerHTML={{ __html: product.description }}
              />
            </div>
            <div className="flex justify-around mt-8">
              <div>
                <p className="text-center">Хугацаа</p> <p>{product.date}</p>
              </div>
              <div>
                <p className="text-center">Насны ангилал</p>
                <p className="text-center">{product.age}</p>
              </div>
            </div>
            <form className="mt-6">
              {/* <div className="sm:flex-col1 mt-10 flex"> */}
              <button
                type="submit"
                className="flex max-w-xs flex-1 items-center justify-center rounded-full border border-transparent bg-[#0EB0EC] px-3 py-2 text-base font-medium text-white sm:w-full"
              >
                Худалдаж авах
              </button>
              {/* </div> */}
            </form>
            <section aria-labelledby="details-heading" className="mt-12">
              {/* <h2 id="details-heading" className="sr-only"> Additional details       </h2> */}
              <div className="divide-y divide-gray-200 border-b">
                {product.details.map((detail) => (
                  <Disclosure as="div" key={detail.name}>
                    {({ open }) => (
                      <>
                        <h3>
                          <Disclosure.Button className="group relative flex w-full items-center justify-between py-6 text-left">
                            <span
                              className={classNames(
                                open ? "text-[#0EB0EC]" : "text-gray-900",
                                "text-sm font-medium"
                              )}
                            >
                              {detail.name}
                            </span>
                            <span className="ml-6 flex items-center">
                              {open ? (
                                <MinusIcon
                                  className="block h-6 w-6 text-[#0EB0EC]"
                                  aria-hidden="true"
                                />
                              ) : (
                                <PlusIcon
                                  className="block h-6 w-6 text-gray-400"
                                  aria-hidden="true"
                                />
                              )}
                            </span>
                          </Disclosure.Button>
                        </h3>
                        <Disclosure.Panel
                          as="div"
                          className="prose prose-sm pb-6"
                        >
                          <p>{detail.description}</p>
                          <ul role="list" className="mt-3">
                            {detail.items.map((item) => (
                              <li key={item}>{item}</li>
                            ))}
                          </ul>
                        </Disclosure.Panel>
                      </>
                    )}
                  </Disclosure>
                ))}
              </div>
              <p className="font-bold mt-12">Төсөлт хөтөлбөр</p>
              <p className="mt-3">
                Зусланд амарч буй хүүхдүүд доорх 5 чиглэлийн 30 гаруй
                хөтөлбөрөөс өөрсдөө сонгон дугуйлан хэлбэрээр хамрагдана. Бүлэг
                үл хамааран өөрийн сонирхлоороо нэгдэн тухайн хөтөлбөрт
                хамрагдана.
              </p>
              <div className="divide-y divide-gray-200 border-b">
                {product.project.map((detail) => (
                  <Disclosure as="div" key={detail.title}>
                    {({ open }) => (
                      <>
                        <h3>
                          <Disclosure.Button className="group relative flex w-full items-center justify-between py-6 text-left">
                            <span
                              className={classNames(
                                open ? "text-indigo-600" : "text-gray-900",
                                "text-sm font-medium"
                              )}
                            >
                              {detail.title}
                            </span>
                            <span className="ml-6 flex items-center">
                              {open ? (
                                <MinusIcon
                                  className="block h-6 w-6 text-indigo-400 group-hover:text-indigo-500"
                                  aria-hidden="true"
                                />
                              ) : (
                                <PlusIcon
                                  className="block h-6 w-6 text-gray-400 group-hover:text-gray-500"
                                  aria-hidden="true"
                                />
                              )}
                            </span>
                          </Disclosure.Button>
                        </h3>
                        <Disclosure.Panel
                          as="div"
                          className="prose prose-sm pb-6"
                        >
                          <ul role="list" className="mt-3 ml-10">
                            {/* {detail.items.map((item) => (       <li key={item}>{item}</li>     ))} */}
                            {detail.items.split("/").map((item) => (
                              <li key={item} className="list-disc">
                                {item}
                              </li>
                            ))}
                          </ul>
                        </Disclosure.Panel>
                      </>
                    )}
                  </Disclosure>
                ))}
              </div>
            </section>
          </div>
        </div>
      </div>
      <div className="bg-white">
        <div className="mx-auto max-w-7xl px-6 py-24 sm:pt-32 lg:px-8 lg:py-40">
          <div className="lg:grid lg:grid-cols-12 lg:gap-8">
            <div className="lg:col-span-5">
              <h2 className="text-2xl font-bold leading-10 tracking-tight text-gray-900">
                Түгээмэл асуулт хариулт
              </h2>
            </div>
            <div className="mt-10 lg:col-span-7 lg:mt-0">
              <dl className="space-y-10">
                {faqs.map((faq) => (
                  <div key={faq.question}>
                    <dt className="text-base font-semibold leading-7 text-gray-900">
                      {faq.question}
                    </dt>
                    <dd className="mt-2 text-base leading-7 text-gray-600">
                      {faq.answer}
                    </dd>
                  </div>
                ))}
              </dl>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
