const posts = [
  // More posts...
  {
    id: 1,
    title: "Байгаль танин мэдэхүй /World Challange/",
    href: "#",
    description:
      "Би байгалийн найз / Хог хаягдалын менежмент / Цэцэгчний хүслэн / Хиймэл хананы авиралт / Уяа зангилаа / Уулын дугуйтай аялал",
    imageUrl: "/baigali.png",
  },
  {
    id: 1,
    title: "Зан заншил, Өв соёл, Уламжлал /Go Nomad/",
    href: "#",
    description:
      "Миний нутаг / Эх хэлний баялаг / Монгол гэр / Оньсон тоглоом  / Төрийн бэлгэдэл / Эрийн гурван наадам / Морин хуур / Бие биелэгээ / Морь унах / Нум сум харвах",
    imageUrl: "/zan-zanshil.png",
  },
  {
    id: 1,
    title: "Амьдрах ухаан, Бүтээлч сэтгэлгээ /My Dream, My Promise/",
    href: "#",
    description:
      "Керамик урлал /  Шүрэн урлал /  Утсан зангилаа /  Модон урлал /  Арьсан урлал /  Санхүүгийн боловсрол",
    imageUrl: "/amidrah.png",
  },
  {
    id: 1,
    title: "Хувь хүний чадвар",
    href: "#",
    description:
      "Ой тогтоолт / Мэдрэмжийн хөгжил / Сахилга, дэг журам / Эрүүл мэнд нөхөн үржихүй / Харилцааны баялаг",
    imageUrl: "/huwi-hun.png",
  },
  {
    id: 1,
    title: "Соёл урлаг, спорт /My Hobby/",
    href: "#",
    description:
      "Хэмнэл  / Гитар, Үкүлэлэ хөгжим  / Бүжиг (Чөлөөт, цэнгээнт)  / Найрамдал медиа /  Спорт өрөлт /  Кендама /  Крикет /  Райгби /  Гар бөмбөг  / Сагсан бөмбөг /  Хөл бөмбөг",
    imageUrl: "/soyl.png",
  },
];

export default function Example() {
  return (
    <div className="bg-white py-24 sm:py-32">
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto max-w-2xl lg:max-w-4xl">
          <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
            Төсөлт хөтөлбөрүүд
          </h2>
          <p className="mt-2 text-lg leading-8 text-gray-600">
            Зусланд амарч буй хүүхдүүд доорх 5 чиглэлийн 30 гаруй хөтөлбөрөөс
            өөрсдөө сонгон дугуйлан хэлбэрээр хамрагдана. Бүлэг үл хамааран
            өөрийн сонирхлоороо нэгдэн тухайн хөтөлбөрт хамрагдана.
          </p>
          <div className="mt-16 space-y-20 lg:mt-20 lg:space-y-20">
            {posts.map((post) => (
              <article
                key={post.id}
                className="relative isolate flex flex-col gap-8 lg:flex-row"
              >
                <div className="relative aspect-[16/9] sm:aspect-[2/1] lg:aspect-square lg:w-64 lg:shrink-0">
                  <img
                    src={post.imageUrl}
                    alt=""
                    className="absolute inset-0 h-full w-full rounded-2xl bg-gray-50 object-cover"
                  />
                  <div className="absolute inset-0 rounded-2xl ring-1 ring-inset ring-gray-900/10" />
                </div>
                <div>
                  <div className="group relative max-w-md">
                    <h3 className="mt-3 text-lg font-semibold leading-6 text-[#003463]">
                      <a href={post.href}>
                        <span className="absolute inset-0" /> {post.title}
                      </a>
                    </h3>
                    <ul className="mt-5 text-sm leading-6 text-[#003463] ml-4">
                      {post.description.split("/").map((item) => (
                        <li key={item} className="list-disc">
                          {item}
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </article>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
