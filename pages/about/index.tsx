import {
  CloudArrowUpIcon,
  LockClosedIcon,
  ServerIcon,
} from "@heroicons/react/20/solid";

import { InboxIcon, TrashIcon, UsersIcon } from "@heroicons/react/24/outline";

import {
  CheckIcon,
  HandThumbUpIcon,
  UserIcon,
} from "@heroicons/react/20/solid";

const timeline = [
  {
    id: 1,
    content: `Зуны болон өвлийн ээлжтэйгээр хүүхдүүдийг хүлээн авx бөгөөд өвлийн ээлжид "Харшийн сүлд модны наадам" хийдэг болсон.`,
    href: "#",
    date: "1978-1981",
    icon: UserIcon,
    iconBackground: "bg-gray-400",
  },
  {
    id: 2,
    content: `Пионерын байгууллагыг дэмждэг зуслан болж, анги хамт олны болон өдрийн амралтын үйлчилгээ нэвтрүүлсэн.`,
    href: "#",
    date: "1990-1992",
    icon: HandThumbUpIcon,
    iconBackground: "bg-blue-500",
  },
  {
    id: 3,
    content:
      "Хурал, симпозиум, аварга шалгаруулах тэмцээн, наадам зэрэг төрөл бүрийн арга хэмжээг зохион байгуулж. Сургуулийн барилгыг “Хөгжил мэдээллийн ордон” болгож шинэчлэв.",
    href: "#",
    date: "1998-2005 ",
    icon: CheckIcon,
    iconBackground: "bg-green-500",
  },
  {
    id: 4,
    content:
      "Зуслангийн үйл ажиллагааны стандартыг боловсруулж, урлагийн наадам, зөвлөгөөний хөтөлбөр зэрэг төрөл бүрийн арга хэмжээг зохион байгуулав. Нээлттэй цахим зуслан хөтөлбөр хэрэгжиж эхлэв.",
    href: "#",
    date: "2006-2010",
    icon: HandThumbUpIcon,
    iconBackground: "bg-blue-500",
  },
  {
    id: 5,
    content:
      "Олон улсын бага хурал, театрын нээлт, Ази Номхон далайн скаутуудын Жембори зэрэг төрөл бүрийн арга хэмжээг зохион байгуулсан. Олон улсын хүүхдийн “Найрамдал” төвийн нэрийг “Олон улсын хүүхдийн “Найрамдал” цогцолбор болгон шинэчлэн өргөтгөн, уламжлалт харилцаагаа дахин сэргээж ОХУ-ын “Артек”, Орлёнок зуслантай  хамтарч ажиллах гэрээ байгууллаа",
    href: "#",
    date: "2011-2020 ",
    icon: CheckIcon,
    iconBackground: "bg-green-500",
  },
];

function classNames(...classes: any[]) {
  return classes.filter(Boolean).join(" ");
}

const featuresContent = [
  {
    name: "Чанарын бодлого",
    description:
      "Улс үндэстний хүүхэд залуучуудыг хөгжилд түүчээлсэн ”Найрамдал хөтөлбөр”-өөр хэрэглэгчдэд таатай найрсаг үйлчилгээгээр салбараа хөгжүүлж, үйлчлүүлэгчдийн амарч хөгжих ая тухтай орчин нөхцлийг бүрдүүлэн, чадварлаг хамт олныг байгууллагын соёлоор удирдаж, Монгол хүний язгуур үнэт зүйлс, түүхэн уламжлалыг өвлүүлэн үлдээхэд бодьтой хувь нэмэр оруулан үйл ажиллагаагаа байнга сайжруулан ажиллана.",
    href: "#",
    icon: InboxIcon,
  },
  {
    name: "Байгаль орчны бодлого",
    description:
      "Бидний үйл ажиллагаанаас үүдэн гарах байгаль орчны бохирдлын үр нөлөөг боломжит хэмжээнд бууруулан, эх дэлхийгээ хайрлан хамгаалах өв соёл, ёс заншлыг ажилтнууд болон хүүхэд залуучуудад түгээн дэлгэрүүлж, хууль эрх зүйн шаардлагыг бүрэн хангаж, байгалийн нөөцийг үр ашигтай хэрэглэж, ногоон байгууламжийг нэмэгдүүлэн, хог хаягдлын ухаалаг тогтолцоог бүрдүүлэн ажиллана.",
    href: "#",
    icon: UsersIcon,
  },
];

const features = [
  {
    name: "Эрхэм зорилго",
    description: "Хүүхэд өсвөр үеийнхний хөгжлийг манлайлан дэмжинэ.",
    icon: CloudArrowUpIcon,
  },
  {
    name: "Үнэт зүйлс",
    description:
      "Уламжлал, Нэр хүнд, Манлайлал, Чанартай үйлчилгээ, Найрсаг харилцаа",
    icon: LockClosedIcon,
  },
  {
    name: "Алсын хараа",
    description: "Азийн загвар зуслан болно.            ",
    icon: ServerIcon,
  },
];

const people = [
  {
    name: "Одод",
    bio: "Скандинавийн орнуудын барилга байгууламжийн хэв маягийг хадгалсан . Хадаас ороогүй оньсон чагтлах аргаар баригдсан .Учир нь уг орон маш чийглэг учраас хадаас зэв болж модоо идэн нурах аюултай учраас төмөрлөг зүйл бага ордог.",
    imageUrl: "/odod.png",
  },
  {
    name: "Солонго",
    imageUrl: "/solongo.png",
    bio: "Зүүн өмнөд ази буюу Вьетнам үндэсний барилгын хэв маягаар баригдсан. Социалист орнуудаас архитекторчид урьсны дагуу Вьетнам улс зураг төслөө илгээсэн. Барилга байшингууд нь ихэвчлэн усан дээр баригдсан байдгаас доороо урт модон тулгууртай байдаг.",
  },
  {
    name: "Сансар",
    imageUrl: "/sansar.png",
    bio: "Эртний Орос тосгоны байшин мэт сэтгэгдэл төрүүлэхээр баригдсан. Эртний Оросууд цонхны ойролцоо дээвэр, ханаа модоор янз бүрийн сийлбэр, хээ угалзуудын хамт хийн чамин маяг оруулдаг байсан. Мөн сүмийн маягтай цамхаг дээр нь зоосон тахиа үүнд хамаарна.",
  },
  {
    name: "Туяа",
    imageUrl: "/tuya.png",
    bio: "Болгар байшин нь нар тусах газар болгон буюу хана болгондоо нарны гэрэл тусгах, агаар оруулах зориулалттай цонх, дотор гаднагүй шавардлагатай байдгаараа онцлогтой билээ.",
  },
  {
    name: "Оч",
    imageUrl: "/och.png",
    bio: "Геометрийн дүрсүүд орсон хурц тод өнгө, 2 давхар баригдсан.",
  },
  {
    name: "Нарлаг",
    imageUrl: "/narlag.png",
    bio: "Монгол загварын байшин эртний сүм дуганы оройн ганжир, чөлөөт цагийн танхим нь Монгол гэр шиг дугуй хэлбэртэй хийгдсэн нь барилгын хийцтэй.",
  },
  {
    name: "Сэтгэмж",
    imageUrl: "/setgemj.png",
    bio: "Өмнө нь ОУХНЦ-н захиргаа, эмнэлэг үйл ажиллагаагаа явуулдаг байгаад Сэтгэмж бүлэг нэртэй болсон.",
  },
  {
    name: "Өвлийн амралтын төв байр",
    imageUrl: "/owliin.png",
    bio: "Өвлийн амралтын төв байр нь Ирээдүй, Мичид , Оргил, Сонор, Нөхөрлөл ,Ундраа гэсэн нийт 6 бүлэгтэй. Бүлгээсээ хамаарч 1 өрөөнд 4-10 хүүхэд амрах бөгөөд нийт 260 хүүхэд хүлээн авах хүчин чадалтай.",
  },
];

export default function About() {
  return (
    <>
      <section className="bg-white">
        <div className="relative mx-auto max-w-7xl py-24 sm:py-32 lg:max-w-5xl">
          <figure className="grid grid-cols-1">
            <div className="relative col-span-2 lg:col-start-1 lg:row-start-2 sm:ml-20">
              <blockquote className="text-xl font-semibold leading-8 text-gray-900 sm:text-2xl sm:leading-9">
                <p className="px-4">Таньд энэ өдрийн мэндийг хүргэе!</p>
              </blockquote>
              <p className="w-full px-4">
                Юуны өмнө Олон улсын хүүхдийн Найрамдал цогцолборын 40 гаруй
                жилийн түүхийг хамтдаа бүтээлцэж өнөөгийн өндөрлөгт хүргэсэн үе
                үеийн ахмад багш, ажилчид, амрагч хүүхдүүд, эцэг эхчүүддээ
                талархал илэрхийлье. Сүүлийн 2 жилийн турш дэлхий дахинд
                нүүрлээд буй цар тахлын нөлөөгөөр сургууль, зуслан төдийгүй
                хүүхдийн төлөө үйл ажиллагаа эрхлэн явуулдаг бүхий л
                байгууллагууд хаалгаа хаагаад удаж байна. Удахгүй дэлхий дахинд
                цар тахлыг ялан дийлж уулзан учрах сайхан цаг ирж амрагч
                хүүхдүүдээ хүлээн авч хүүхдийн гүрэнт улс Найрамдалд дахин
                хүүхдийн инээд, дуу хуур цангинах тэрхүү өдрийг тэсэн ядан
                хүлээж хамт олон маань зуны ээлжээ хүлээн авах бэлтгэл ажилдаа
                шамдан ороод байна. Бид өөрчлөлт, шинэчлэлийн эрин зуунд амьдарч
                байна. Энэ цаг үед шинэчлэл, өөрчлөлттэй хөл нийлүүлж хүүхэд
                өсвөр үеийнхнийхээ танин мэдэх сэтгэн бодох, эрүүл ахуйн зөв
                дадал, хэвшилтэй болох, байгаль орчинтойгоо зөв харьцах, хамтдаа
                суралцаж хамтдаа хөгжих шинэлэг бөгөөд сонирхолтой арга
                хэлбэрийг манай хамт олон эрэлхийлж хөгжлийн хөтөлбөрөө
                сайжруулж орчин цагийн хүүхэд өсвөр үеийнхнийхээ хэрэгцээ
                шаардлагад бүрэн нийцсэн үйл ажиллагаагаараа бүс нутагтаа
                тэргүүлэх зуслан болохоор зорилт тавин ажиллаж байна. Хүүхдийн
                эрхэд суурилсан хүүхдэд ээлтэй байх бүх нийтийн үйлс дэлгэрэх
                болтугай.
              </p>
            </div>
            <div className="col-end-1 lg:row-span-4 sx:w-32">
              <img
                className="rounded-xl bg-indigo-50 lg:rounded-3xl ml-7 mt-8 lg:mt-0"
                src="/otgonbat.png"
                alt=""
              />
              <figcaption className="text-base flex-col items-center justify-center">
                <div className="mt-1 text-gray-500 text-center">
                  Ерөнхий захирал
                </div>
                <div className="font-semibold text-gray-900 text-center">
                  Б. Отгонбат
                </div>
              </figcaption>
            </div>
          </figure>
        </div>
      </section>
      {/* Section 2 */}
      <div className="overflow-hidden bg-white py-24 sm:py-32">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          <div className="mx-auto grid max-w-2xl grid-cols-1 gap-x-8 gap-y-16 sm:gap-y-20 lg:mx-0 lg:max-w-none lg:grid-cols-2">
            <div className="lg:ml-auto lg:pl-4 lg:pt-4">
              <div className="lg:max-w-lg">
                <p className="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
                  Цогцолборын танилцуулга
                </p>
                <p className="mt-6 text-lg leading-8 text-gray-600">
                  Тус цогцолбор нь Засгийн газрын хэрэгжүүлэгч агентлаг-Гэр бүл,
                  хүүхэд, залуучуудын хөгжлийн газрын харъяа Чанарын
                  менежментийн тогтолцоо ISO 9001:2015/MNS ISO 9001:2016
                  стандартын шаардлагыг бүрэн нэвтрүүлсэн төрийн үйлчилгээний
                  байгууллага юм.
                </p>
                <dl className="mt-10 max-w-xl space-y-8 text-base leading-7 text-gray-600 lg:max-w-none">
                  {features.map((feature) => (
                    <div key={feature.name} className="relative pl-9">
                      <div className="inline font-semibold text-gray-900">
                        <feature.icon
                          className="absolute left-1 top-1 h-5 w-5 text-indigo-600"
                          aria-hidden="true"
                        />
                        {feature.name}
                      </div>
                      <div>{feature.description}</div>
                    </div>
                  ))}
                </dl>
              </div>
            </div>
            <div className="flex items-start justify-end lg:order-first">
              <img
                src="/nairamdal-image.png"
                alt="Product screenshot"
                className="w-[48rem] max-w-none rounded-xl shadow-xl ring-1 ring-gray-400/10 sm:w-[57rem]"
                width={2432}
                height={1442}
              />
            </div>
          </div>
        </div>
      </div>
      {/* Section 3 */}
      <div className="bg-white py-24 sm:py-32">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          <div className="mx-auto mt-16 max-w-2xl sm:mt-20 lg:mt-24 lg:max-w-none">
            <dl className="grid max-w-xl grid-cols-1 gap-x-8 gap-y-16 lg:max-w-none lg:grid-cols-2">
              {featuresContent.map((feature) => (
                <div key={feature.name} className="flex flex-col">
                  <dt className="text-base font-semibold leading-7 text-gray-900">
                    <div className="mb-6 flex h-10 w-10 items-center justify-center rounded-lg bg-indigo-600">
                      <feature.icon
                        className="h-6 w-6 text-white"
                        aria-hidden="true"
                      />
                    </div>
                    {feature.name}
                  </dt>
                  <dd className="mt-1 flex flex-auto flex-col text-base leading-7 text-gray-600">
                    <p className="flex-auto">{feature.description}</p>
                  </dd>
                </div>
              ))}
            </dl>
          </div>
        </div>
      </div>

      {/* Section 4 */}

      <main>
        <div className="relative isolate">
          <svg
            className="absolute inset-x-0 top-0 -z-10 h-[64rem] w-full stroke-gray-200 [mask-image:radial-gradient(32rem_32rem_at_center,white,transparent)]"
            aria-hidden="true"
          >
            <defs>
              <pattern
                id="1f932ae7-37de-4c0a-a8b0-a6e3b4d44b84"
                width={200}
                height={200}
                x="50%"
                y={-1}
                patternUnits="userSpaceOnUse"
              >
                <path d="M.5 200V.5H200" fill="none" />
              </pattern>
            </defs>
            <svg x="50%" y={-1} className="overflow-visible fill-gray-50">
              <path
                d="M-200 0h201v201h-201Z M600 0h201v201h-201Z M-400 600h201v201h-201Z M200 800h201v201h-201Z"
                strokeWidth={0}
              />
            </svg>
            <rect
              width="100%"
              height="100%"
              strokeWidth={0}
              fill="url(#1f932ae7-37de-4c0a-a8b0-a6e3b4d44b84)"
            />
          </svg>
          <div
            className="absolute left-1/2 right-0 top-0 -z-10 -ml-24 transform-gpu overflow-hidden blur-3xl lg:ml-24 xl:ml-48"
            aria-hidden="true"
          >
            <div
              className="aspect-[801/1036] w-[50.0625rem] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30"
              style={{
                clipPath:
                  "polygon(63.1% 29.5%, 100% 17.1%, 76.6% 3%, 48.4% 0%, 44.6% 4.7%, 54.5% 25.3%, 59.8% 49%, 55.2% 57.8%, 44.4% 57.2%, 27.8% 47.9%, 35.1% 81.5%, 0% 97.7%, 39.2% 100%, 35.2% 81.4%, 97.2% 52.8%, 63.1% 29.5%)",
              }}
            />
          </div>
          <div className="overflow-hidden">
            <div className="mx-auto max-w-7xl px-6 pb-32 pt-36 sm:pt-60 lg:px-8 lg:pt-32">
              <div className="mx-auto max-w-2xl gap-x-14 lg:mx-0 lg:flex lg:max-w-none lg:items-center">
                <div className="w-full max-w-xl lg:shrink-0 xl:max-w-2xl">
                  <div className="flow-root">
                    <ul role="list" className="-mb-8">
                      {timeline.map((event, eventIdx) => (
                        <li key={event.id}>
                          <div className="relative pb-8">
                            {eventIdx !== timeline.length - 1 ? (
                              <span
                                className="absolute left-4 top-4 -ml-px h-full w-0.5 bg-gray-200"
                                aria-hidden="true"
                              />
                            ) : null}
                            <div className="relative flex space-x-3">
                              <div>
                                <span
                                  className={
                                    // event.iconBackground,
                                    "h-8 w-8 rounded-full flex items-center justify-center ring-8 ring-white bg-[#16ADEB]"
                                  }
                                >
                                  {/* <event.icon
                                    className="h-5 w-5 text-white"
                                    aria-hidden="true"
                                  /> */}
                                </span>
                              </div>
                              <div className="flex min-w-0 flex-1 justify-between space-x-4 pt-1.5">
                                <div>
                                  <p className="text-md ">
                                    {event.date}
                                    {/* <a
                                      href={event.href}
                                      className="font-medium text-gray-900"
                                    >
                                      {event.target}
                                    </a> */}
                                  </p>
                                  <p className="text-gray-500">
                                    {event.content}
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
                <div className="mt-14 flex justify-end gap-8 sm:-mt-44 sm:justify-start sm:pl-20 lg:mt-0 lg:pl-0">
                  <div className="ml-auto w-44 flex-none space-y-8 pt-32 sm:ml-0 sm:pt-80 lg:order-last lg:pt-36 xl:order-none xl:pt-80">
                    <div className="relative">
                      <img
                        src="/Rectangle_37.png"
                        alt=""
                        className="aspect-[2/3] w-full rounded-xl bg-gray-900/5 object-cover shadow-lg"
                      />
                      <div className="pointer-events-none absolute inset-0 rounded-xl ring-1 ring-inset ring-gray-900/10" />
                    </div>
                  </div>
                  <div className="mr-auto w-44 flex-none space-y-8 sm:mr-0 sm:pt-52 lg:pt-36">
                    <div className="relative">
                      <img
                        src="/Rectangle_38.png"
                        alt=""
                        className="aspect-[2/3] w-full rounded-xl bg-gray-900/5 object-cover shadow-lg"
                      />
                      <div className="pointer-events-none absolute inset-0 rounded-xl ring-1 ring-inset ring-gray-900/10" />
                    </div>
                    <div className="relative">
                      <img
                        src="/Rectangle_39.png"
                        alt=""
                        className="aspect-[2/3] w-full rounded-xl bg-gray-900/5 object-cover shadow-lg"
                      />
                      <div className="pointer-events-none absolute inset-0 rounded-xl ring-1 ring-inset ring-gray-900/10" />
                    </div>
                  </div>
                  <div className="w-44 flex-none space-y-8 pt-32 sm:pt-0">
                    <div className="relative">
                      <img
                        src="/Rectangle_41.png"
                        alt=""
                        className="aspect-[2/3] w-full rounded-xl bg-gray-900/5 object-cover shadow-lg"
                      />
                      <div className="pointer-events-none absolute inset-0 rounded-xl ring-1 ring-inset ring-gray-900/10" />
                    </div>
                    <div className="relative">
                      <img
                        src="/Rectangle_42.png"
                        alt=""
                        className="aspect-[2/3] w-full rounded-xl bg-gray-900/5 object-cover shadow-lg"
                      />
                      <div className="pointer-events-none absolute inset-0 rounded-xl ring-1 ring-inset ring-gray-900/10" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>

      {/* section 6 */}

      <div className="bg-white py-24 sm:py-32">
        <div className="mx-auto max-w-7xl px-6 lg:px-8">
          <div className="mx-auto max-w-3xl sm:text-center">
            <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
              Бүлгийн танилцуулга
            </h2>
            <p className="mt-6 text-lg leading-8 text-gray-600">
              Найрамдал зуслан нь зуны болон өвлийн гэсэн нийт 8 бүлэгтэй. Зун
              үйл ажиллагаагаа явуулдаг 8 бүлэгтэй бол өвөл үйл ажиллагаагаа
              явуулдаг 6 бүлэгтэй.
            </p>
          </div>
          <ul
            role="list"
            className="mx-auto mt-20 grid max-w-2xl grid-cols-1 gap-x-6 gap-y-20 sm:grid-cols-2 lg:max-w-4xl lg:gap-x-8 xl:max-w-none"
          >
            {people.map((person) => (
              <li key={person.name} className="flex flex-col gap-6 xl:flex-row">
                <img
                  className="aspect-[4/5] w-52 flex-none rounded-2xl object-cover"
                  src={person.imageUrl}
                  alt=""
                />
                <div className="flex-auto">
                  <h3 className="text-lg font-semibold leading-8 tracking-tight text-gray-900">
                    {person.name}
                  </h3>
                  <p className="mt-6 text-base leading-7 text-gray-600">
                    {person.bio}
                  </p>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </>
  );
}
